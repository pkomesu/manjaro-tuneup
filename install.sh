#!/usr/bin/env bash
{
    printf '\n'
	echo 'Seteando mirror......'
	sudo pacman-mirrors --fasttrack
	
	echo 'Actualizando paquetes...'
	sudo pacman -Syu --noconfirm
	echo 'Paquetes actualizados'
	
	sudo pacman -S git --noconfirm
	sudo pacman -S curl --noconfirm
	sudo pacman -S dnsutils --noconfirm

	sudo pacman -S yay --noconfirm
	sudo pacman -S snapd --noconfirm
	sudo snap install teams
	#yay -S teams --noconfirm
	sudo pacman -S flameshot --noconfirm
	yay -S google-chrome --noconfirm
	yay -S foxitreader --noconfirm

	sudo pacman -S openjdk-src --noconfirm
	sudo pacman -S maven --noconfirm
	yay -S eclipse-jee --noconfirm
	yay -S lombok-eclipse-jee --noconfirm
	yay -S code --noconfirm
	yay -S intellij-idea-community-edition --noconfirm
	yay -S sublime-text-4 --noconfirm
	yay -S postman --noconfirm
	sudo pacman -S pgadmin4 --noconfirm
	sudo pacman -S dbeaver --noconfirm
	yay -S ngrok --noconfirm
	yay -S staruml --noconfirm
	yay -S yed --noconfirm
	sudo pacman -S nodejs npm --noconfirm

	yay -S docker --noconfirm #probar sudo pacman -S docker --noconfirm
	sudo groupadd docker
	sudo usermod -a -G docker $USER
	sudo systemctl restart docker

	# Commitizen
	sudo pacman -S python --noconfirm
	sudo pacman -S python-pip --noconfirm
	sudo pip install -U pre-commit
	sudo pip install -U Commitizen

	yay -S kronometer --noconfirm
	yay -S teamviewer --noconfirm
	sudo pacman -S discord --noconfirm
}

#teams --no-sandbox --disable-gpu-sandbox
#teams /usr/bin/teams %U ### Startup teams in manjaro

#Agregar nombre del branch en la ruta actual de bash
#Agregar en archivo .bashrc
# Git branch in prompt.
#git_branch() {
#  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
#}

#export PS1="\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$(git_branch)\[\033[00m\] $ "


# SimpleScreenRecorder